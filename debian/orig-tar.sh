#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=ohcount-$2
TAR=../ohcount_$2.orig.tar.gz

# clean up the upstream tarball
tar zxf $3 
# Remove vendor/ because it is including 
tar -c -z -X debian/orig-tar.exclude -f $TAR $DIR/
rm -rf $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
    . .svn/deb-layout
    mv $TAR $origDir
    echo "moved $TAR to $origDir"
fi

